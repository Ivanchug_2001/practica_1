﻿
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Net.Http;

namespace Practica_1
{
    class Program
    {
        static void Main(string[] args)
        {
 
        }

        public static void Ejercicio1()
        {
            int aprob = 0;
            int sobre = 0;
            double nota;

            for (int i = 0; i < 10; i++)
            {
                nota = double.Parse(Console.ReadLine());
                if (nota >= 9)
                {
                    sobre++;
                }
                else if (nota >= 5)
                {
                    aprob++;
                    aprob = aprob + sobre;
                }

            }
            Console.WriteLine("Los sobrasliente fueron " + sobre);
            Console.WriteLine("Los aprobados fueron " + aprob);
        }

        public static void Ejercicio2()
        {
            int dia, mes, anio, mesnuevo, aux;
            dia = Int32.Parse(Console.ReadLine());
            mes = Int32.Parse(Console.ReadLine());
            anio = Int32.Parse(Console.ReadLine());

            if (dia < 1 || dia > 30 || mes > 12 || mes < 1)
            {
                Console.WriteLine("No has insertado la fecha correctamente");
            }
            else
            {
                mesnuevo = mes + 3;
                if (mesnuevo > 12)
                {
                    aux = mesnuevo - 12;
                    mesnuevo = aux;
                    anio++;
                }
                Console.WriteLine("la fecha de pago es: " + dia + "/" + mesnuevo + "/" + anio);
            }

        }

        public static void Ejercicio3()
        {
            int[] numeros = new int[10];
            for (int i = 0; i < numeros.Length; i++)
            {

                numeros[i] = Int32.Parse(Console.ReadLine());
            }
            Console.WriteLine("");
            Console.WriteLine("Los numeros son");
            Console.WriteLine("");
            for (int i = 0; i < numeros.Length; i++)
            {
                Console.WriteLine(numeros[i] + " su posicion " + (i + 1));
            }
            Console.WriteLine("");
            Console.WriteLine("Y el orden alreves es");
            Console.WriteLine("");
            int e = 10;
            while (e != 0)
            {
     
                for (int i = 9; i >= 0; i--)
                {
                    Console.WriteLine(numeros[i] + " posicion " + (i + 1));
                    e--;
                }
            }
        }
        public static void ejercicio5()
        {
            int num1 = 0;
            int num2 = 0;
            int i = 0;
            int z = 0;
            int[] numeros = new int[10];
            numeros[0] = Int32.Parse(Console.ReadLine());
            numeros[1] = Int32.Parse(Console.ReadLine());
            numeros[2] = Int32.Parse(Console.ReadLine());
            numeros[3] = Int32.Parse(Console.ReadLine());
            numeros[4] = Int32.Parse(Console.ReadLine());
            numeros[5] = Int32.Parse(Console.ReadLine());
            numeros[6] = Int32.Parse(Console.ReadLine());
            numeros[7] = Int32.Parse(Console.ReadLine());
            numeros[8] = Int32.Parse(Console.ReadLine());
            numeros[9] = Int32.Parse(Console.ReadLine());
            while (i < 10)
            {
                if (numeros[i] > num1)
                {
                    num1 = numeros[i];
                    z = i;
                }
                i++;
            }
            Array.Clear(numeros, z, 1);
            i = 0;
            while (i < 10)
            {
                if (numeros[i] > num2)
                {
                    num2 = numeros[i];
                }
                i++;
            }
            Console.WriteLine("Los números más altos son: " + num1 + " " + num2);
        }

        public static void Ejercicio4()
        {
            string[] Empresa = new string[10];
            int[] ingresos = new int[10];
            int[] gastos = new int[10];
            int[] Diferencia = new int[10];
            double menor = 0;
            int loc = 0;

            for (int i = 0; i < Empresa.Length; i++)
            {
                Empresa[i] = "Empresa " + (i + 1);
                Console.WriteLine(Empresa[i]);
                Console.WriteLine("Coloque ganancias del mes");
                ingresos[i] = int.Parse(Console.ReadLine());
                Console.WriteLine("Coloque gastos del mes");
                gastos[i] = int.Parse(Console.ReadLine());
                Diferencia[i] = ingresos[i] - gastos[i];
                if (Diferencia[i] < menor)
                {
                    menor = Diferencia[i];
                    loc = i + 1;
                }
                Console.WriteLine("");
            }
            Console.WriteLine("Empresa " + loc);
            Console.WriteLine("Ha tenido resultado ingreso-gastos, con una perdida de");
            Console.WriteLine(menor);

        }

        public static void Ejercicio7()
        {
            int[] notas = new int[10];
            int mayor = 0;
            int menor = 0;
            double media = 0;
            for (int i = 0; i < notas.Length; i++)
            {
                menor = notas[0];
                Console.WriteLine("Coloque la nota " + (i + 1));
                notas[i] = int.Parse(Console.ReadLine());
                if (notas[i] > mayor)          
                {
                    mayor = notas[i];
                }
                if ((notas[i] < mayor) && (notas[i] < menor))          
                {
                    menor = notas[i];
                }
            }
            if (mayor - menor >= 3)
            {
                Console.WriteLine("Se destimo la nota mayor y la menor debido a su diferencia de 3 puntos");
                media = notas.Sum() - mayor;
                media = media - menor;
                media = media / 8;
                Console.WriteLine("La media es " + media);
            }
            else
            {
                media = notas.Sum() / 10;
                Console.WriteLine("La media es " + media);
            }
        }
    }
}